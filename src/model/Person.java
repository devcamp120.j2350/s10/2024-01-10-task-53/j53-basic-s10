package model;

public class Person {
    String FirstName;
    String LastName;
    String Gender;
    String Contact;
    String Email;

    public Person() {
    }

    public Person(String firstName, String lastName, String gender, String contact, String email) {
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        Contact = contact;
        Email = email;
    }

    public Person(String firstName, String lastName, String gender) {
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Person[Firstname = " + this.FirstName + " " + this.LastName + "]";
    }
    
}

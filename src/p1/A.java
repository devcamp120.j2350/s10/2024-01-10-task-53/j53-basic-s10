package p1;

public class A {
    private int i;
    int j;//default
    protected int k;
    public int l;

    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.l);
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }
    
}

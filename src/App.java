import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Person person1 = new Person("Nguyen", "Van Nam", "Nam", "Ha Noi", "namvn@gmail.com");        
        System.out.println(person1.getFirstName());
        System.out.println(person1.getLastName());
        System.out.println(person1.getEmail());
        person1.setEmail("namnguyen@gmail.com");
        System.out.println(person1.getEmail());
        System.out.println(person1);

        Person person2 = new Person();
        person2.setFirstName("Le");
        person2.setLastName("Thanh Binh");
        System.out.println(person2.getFirstName());
        System.out.println(person2.getLastName());
        System.out.println(person2.toString());

        Person person3 = new Person("Tran", "Van Thang", "nam");
        System.out.println(person3.toString());
    }
}
